import 'package:flutter/material.dart';
import 'package:moviedownloader/src/model/customMeterialColor.dart';
import 'package:moviedownloader/src/view/widget/movieCard.dart';

import '../model/movie.dart';
import '../search/customSearchDelegate.dart';

List<Movie> movieList = new List();

class MovieList extends StatefulWidget {
  final List<Movie> movieData;
  final String title;

  MovieList({this.movieData, this.title});

  static const String routeName = '/movie/list';

  @override
  MovieListState createState() =>
      new MovieListState(movieData: movieData, pageTitle: title);
}

class MovieListState extends State<MovieList> {
  final List<Movie> movieData;
  final String pageTitle;

  MovieListState({this.movieData, this.pageTitle});

  void search() {
    showSearch(
      context: context,
      delegate: CustomSearchDelegate(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(pageTitle),
          actions: <Widget>[
            new IconButton(icon: const Icon(Icons.search), onPressed: search),
          ],
        ),
        body: _buildMovieCard(movieData),
        backgroundColor: CustomColor.backgroundColor);
  }

  Widget _buildMovieCard(List<Movie> movieList) {
    var size = MediaQuery.of(context).size;
    return GridView.count(
      crossAxisCount: 3,
      padding: const EdgeInsets.all(20.0),
      childAspectRatio: (size.width / 580),
      children: List.generate(movieList.length, (index) {
        return new MovieCard(movie: movieList[index]);
      }),
    );
  }

}
