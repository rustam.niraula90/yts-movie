import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moviedownloader/src/model/customMeterialColor.dart';
import 'package:moviedownloader/src/model/genre.dart';
import 'package:moviedownloader/src/model/staticMovieData.dart';
import 'package:moviedownloader/src/search/customSearchDelegate.dart';
import 'package:moviedownloader/src/view/MovieList.dart';
import 'package:moviedownloader/src/view/widget/movieSlider.dart';

class RecentMovies extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new RecentMoviesState();
}

class RecentMoviesState extends State<RecentMovies> {
  void search() {
    showSearch(
      context: context,
      delegate: CustomSearchDelegate(),
    );
  }
  void openMovieList(String genre) {
    Navigator.push(context, MaterialPageRoute(
      builder: (context)=>MovieList(
        movieData: new StaticMovieData().loadMovieByGenreForList(genre),
        title: genre,
      ),
      settings: RouteSettings(name: '/movie/list')
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Recent Movies"),
        actions: <Widget>[
          new IconButton(icon: const Icon(Icons.search), onPressed: search),
        ],
      ),
      body: ListView.separated(
        separatorBuilder: (context, index) {
          if (index == Genre.getAllGenre().length) {
            return Container();
          }
          return getSeparator(Genre.getAllGenre()[index]);
        },
        padding: const EdgeInsets.all(15),
        itemCount: Genre.getAllGenre().length + 1,
        itemBuilder: (context, index) {
          if (index == 0) {
            return Container();
          }
          return getCarousel(Genre.getAllGenre()[index - 1]);
        },
      ),
      backgroundColor: CustomColor.backgroundColor,
//      drawer: CustomDrawer.getDrawer(),
    );
  }

  Widget getSeparator(String genre) {
    return Card(
      color: CustomColor.cardColor,
      child: InkWell(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              child: Text(
                genre,
                style: TextStyle(fontSize: 20.0, color: CustomColor.textColor),
              ),
              padding: EdgeInsets.only(left: 10.0),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: IconButton(
                icon: Icon(Icons.arrow_forward, color: CustomColor.textColor),
                onPressed: () => openMovieList(genre),
              ),
            )
          ],
        ),
        onTap: () => openMovieList(genre),
      ),
    );
  }

  Widget getCarousel(String genre) {
    return Container(
        child: MovieSlider(
      movieList: new StaticMovieData().loadMovieByGenreForDashboard(genre),
      parentContext: context,
    ));
  }
}