import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:moviedownloader/src/model/customMeterialColor.dart';
import 'package:moviedownloader/src/model/staticMovieData.dart';
import 'package:moviedownloader/src/service/torrentService.dart';
import 'package:moviedownloader/src/service/urlLaunchService.dart';
import 'package:moviedownloader/src/view/widget/movieSlider.dart';

import '../model/movie.dart';

class MovieDetail extends StatefulWidget {
  static const String routeName = '/movie/detail';
  final Movie movie;

  MovieDetail({this.movie});

  @override
  MovieDetailState createState() => MovieDetailState(movie: movie);
}

// Copyright 2015 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

class _DetailCategory extends StatelessWidget {
  const _DetailCategory({Key key, this.children}) : super(key: key);

  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: DefaultTextStyle(
        style: Theme.of(context).textTheme.subhead,
        child: SafeArea(
          top: false,
          bottom: false,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.symmetric(vertical: 15.0),
              ),
              Expanded(child: Column(children: children))
            ],
          ),
        ),
      ),
    );
  }
}

class _DetailItem extends StatelessWidget {
  _DetailItem({Key key, this.title, this.info});

  final String title;
  final String info;

  @override
  Widget build(BuildContext context) {
    final List<Widget> rowChildren = <Widget>[
      Expanded(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 5.0),
              child:
                  Text(title, style: TextStyle(color: CustomColor.textColor)),
            ),
            Divider(
              color: CustomColor.dividerColor,
            ),
            Text(info, style: TextStyle(color: CustomColor.textColor))
          ]))
    ];
    return MergeSemantics(
      child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 1.0),
          child: Container(
              child: Card(
                  color: CustomColor.cardColor,
                  child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 16.0, horizontal: 16.0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: rowChildren))))),
    );
  }
}

class _TorrentItem extends StatelessWidget {
  _TorrentItem({Key key, this.movie});

  final Movie movie;

  @override
  Widget build(BuildContext context) {
    final List<Widget> rowChildren = <Widget>[
      Expanded(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
            Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: Text("Downloads",
                style: TextStyle(color: CustomColor.textColor)),
          ),
          Divider(
            color: CustomColor.dividerColor,
          ),
          Column(
              children: movie.torrents.map<Widget>((torrent) {
            return Row(
              children: <Widget>[
                Container(
                  width: 150.0,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 5.0, horizontal: 10.0),
                          child: Text(
                              torrent.quality + " (" + torrent.type + ")",
                              style: TextStyle(color: CustomColor.textColor)),
                        ),
                        Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 5.0, horizontal: 15.0),
                            child: Row(children: <Widget>[
                              Icon(Icons.folder, color: CustomColor.textColor),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10.0),
                                child: Text(torrent.size,
                                    style: TextStyle(
                                        color: CustomColor.textColor)),
                              )
                            ])),
                        Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 5.0, horizontal: 15.0),
                            child: Row(children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(left: 2.0, right: 2.0),
                                child: Text(
                                  "S/P",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: CustomColor.textColor),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10.0),
                                child: Text(torrent.seeds + "/" + torrent.peers,
                                    style: TextStyle(
                                        color: CustomColor.textColor)),
                              )
                            ])),
                        Divider(
                          color: CustomColor.dividerColor,
                        ),
                      ]),
                ),
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Ink(
                        decoration: const ShapeDecoration(
                          color: Colors.white,
                          shape: CircleBorder(),
                        ),
                        child: IconButton(
                          icon: Icon(Icons.file_download),
                          color: Colors.black,
                          onPressed: () {
                            TorrentService.magnetTorrentLink(torrent.hash,
                                movie.title + " " + torrent.quality);
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            );
          }).toList())
        ]),
      )
    ];
    return MergeSemantics(
      child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 1.0),
          child: Container(
              child: Card(
                  color: CustomColor.cardColor,
                  child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 16.0, horizontal: 16.0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: rowChildren))))),
    );
  }
}

class _DetailItemIcon extends StatelessWidget {
  final List<Widget> rows;

  _DetailItemIcon({this.rows});

  @override
  Widget build(BuildContext context) {
    return MergeSemantics(
      child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 0.0),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                      Row(children: rows.sublist(0, rows.length - 1)),
                      rows.last
                    ]))
              ])),
    );
  }
}

class _Genre extends StatelessWidget {
  _Genre({this.genre});

  final String genre;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(children: <Widget>[
          Text(genre, style: TextStyle(color: CustomColor.textColor))
        ]));
  }
}

class _DetailItemIconRow extends StatelessWidget {
  _DetailItemIconRow({this.text, this.icon});

  final String text;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        IconButton(
            icon: Icon(
          icon,
          color: CustomColor.textColor,
        )),
        Text(
          text,
          style: TextStyle(color: CustomColor.textColor),
        )
      ],
    );
  }
}

class MovieDetailState extends State<MovieDetail> {
  final Movie movie;

  MovieDetailState({this.movie});

  final double _appBarHeight = 600.0;
  bool showWatchTrailer = true;
  bool isScrollAnimated = false;
  ScrollController _scrollController = new ScrollController();

  @override
  Widget build(BuildContext context) {
    String genreString = movie.genres.toString();
    genreString = genreString
        .replaceAll("[", "")
        .replaceAll("]", "")
        .replaceAll(",", "/");
    if (!isScrollAnimated) {
      new Timer(const Duration(milliseconds: 100), () {
        isScrollAnimated = true;
        Future animated = _scrollController.animateTo(
          _scrollController.position.maxScrollExtent / 2,
          curve: Curves.easeOut,
          duration: const Duration(milliseconds: 500),
        );
        animated.whenComplete(() {
          _scrollController.addListener(() {
            if (showWatchTrailer) {
              setState(() {
                showWatchTrailer = false;
              });
            }
          });
        });
      });
    }
    return Theme(
      data: ThemeData(
        brightness: Brightness.light,
        primarySwatch: CustomColor.primaryBlack,
        platform: Theme.of(context).platform,
      ),
      child: Scaffold(
        body: CustomScrollView(
          controller: _scrollController,
          slivers: <Widget>[
            SliverAppBar(
              title: Text(movie.title),
              expandedHeight: _appBarHeight,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                //title: Text(movie.title),
                background: Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    Hero(
                        tag: movie.id,
                        child: Image.network(
                          movie.large_cover_image,
                          fit: BoxFit.cover,
                          height: _appBarHeight,
                        )),
                    // This gradient ensures that the toolbar icons are distinct
                    // against the background image.
                    const DecoratedBox(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment(0.0, -1.0),
                          end: Alignment(0.0, -0.4),
                          colors: <Color>[Color(0x60000000), Color(0x00000000)],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(<Widget>[
                _DetailCategory(children: <Widget>[
                  _DetailItemIcon(rows: <Widget>[
                    _DetailItemIconRow(
                        text: movie.year, icon: Icons.calendar_today),
                    _DetailItemIconRow(
                        text: movie.runtime + " min", icon: Icons.access_time),
                    _Genre(genre: genreString)
                  ]),
                ]),
                Divider(
                  color: CustomColor.dividerColor,
                ),
                _DetailCategory(children: <Widget>[
                  Row(children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: Image.asset("assets/images/imdb_logo.png",
                            width: 50.0)),
                    Text(
                      movie.rating,
                      style: TextStyle(color: CustomColor.textColor),
                    ),
                    Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 2.0),
                        child: Icon(
                          Icons.star,
                          size: 15.0,
                          color: CustomColor.textColor,
                        )),
                  ])
                ]),
                _DetailCategory(children: <Widget>[
                  _DetailItem(
                      title: 'Description', info: movie.description_full),
                ]),
                _DetailCategory(
                  children: <Widget>[_TorrentItem(movie: movie)],
                ),
                _suggestion()
              ]),
            ),
          ],
        ),
        floatingActionButton: Stack(
          children: <Widget>[
            new Visibility(
                visible: showWatchTrailer && movie.yt_trailer_code != "",
                child: FloatingActionButton.extended(
                  onPressed: () {
                    UrlLaunchService.playTrailer(movie.yt_trailer_code);
                  },
                  icon: Icon(Icons.play_circle_outline),
                  label: Text("Watch Trailer"),
                  backgroundColor: Colors.red,
                )),
            new Visibility(
                visible: !showWatchTrailer && movie.yt_trailer_code != "",
                child: FloatingActionButton(
                  onPressed: () {
                    UrlLaunchService.playTrailer(movie.yt_trailer_code);
                  },
                  child: Icon(Icons.play_circle_outline),
                  backgroundColor: Colors.red,
                ))
          ],
        ),
        backgroundColor: CustomColor.backgroundColor,
      ),
    );
  }

  Widget _suggestion() {
    return new MovieSlider(
      movieList:
          new StaticMovieData().getMovieSuggestion(movie.genres, 20, movie.id),
      parentContext: context,
      replaceContent: true,
    );
  }
}
