import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moviedownloader/src/model/movie.dart';
import 'package:moviedownloader/src/view/widget/movieImage.dart';

import '../movieDetail.dart';

class MovieCard extends StatefulWidget {
  final Movie movie;

  MovieCard({this.movie});

  @override
  State<StatefulWidget> createState() => new MovieCardState(movie: movie);
}

class MovieCardState extends State<MovieCard> {
  final Movie movie;

  MovieCardState({this.movie});

  @override
  Widget build(BuildContext context) {
    return InkWell(
        child: Container(
          margin: EdgeInsets.all(5.0),
          child: Hero(
              tag: movie.id,
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  child: new MovieImage(imageUrl: movie.large_cover_image))),
        ),
        onTap: () {
          setState(() {
            _showMovieDetail(movie);
          });
        });
  }

  void _showMovieDetail(Movie movie) {
    Navigator.of(context)
        .push(new MaterialPageRoute<void>(builder: (BuildContext context) {
      return MovieDetail(movie: movie);
    }));
  }
}
