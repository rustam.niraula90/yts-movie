import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moviedownloader/src/model/customMeterialColor.dart';
import 'package:moviedownloader/src/model/movie.dart';

import '../movieDetail.dart';

class MovieSlider extends StatefulWidget {
  final List<Movie> movieList;
  final BuildContext parentContext;
  final bool replaceContent;

  MovieSlider(
      {this.movieList, this.parentContext, this.replaceContent = false});

  @override
  State<StatefulWidget> createState() => new MovieSliderState(
      movieList: movieList,
      parentContext: parentContext,
      replaceContent: replaceContent);
}

class MovieSliderState extends State<MovieSlider> {
  final List<Movie> movieList;
  final BuildContext parentContext;
  final bool replaceContent;

  MovieSliderState({this.movieList, this.parentContext, this.replaceContent});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CarouselSlider.builder(
        viewportFraction: 0.39,
        enlargeCenterPage: true,
        autoPlay: false,
        height: 200,
        itemCount: movieList.length,
        itemBuilder: (BuildContext context, int index) =>
            Container(child: _buildMovieCard(movieList[index])));
  }

  Widget _buildMovieCard(Movie suggestionMovie) {
    return InkWell(
        child: Container(
          margin: EdgeInsets.all(5.0),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            child: Image.network(
              suggestionMovie.large_cover_image,
              fit: BoxFit.cover,
              height: 200,
              width: 400,
              loadingBuilder: (BuildContext ctx, Widget child,
                  ImageChunkEvent loadingProgress) {
                if (loadingProgress == null) {
                  return child;
                } else {
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(
                          CustomColor.progressColor),
                    ),
                  );
                }
              },
            ),
          ),
        ),
        onTap: () {
          setState(() {
            _showMovieDetail(suggestionMovie);
          });
        });
  }

  void _showMovieDetail(Movie movie) {
    if (replaceContent) {
      Navigator.pushReplacement(context, MaterialPageRoute(
          builder: (context)=>MovieDetail(movie: movie),
          settings: RouteSettings(name: '/movie/detail')
      ));
    } else {
      Navigator.push(context, MaterialPageRoute(
          builder: (context)=>MovieDetail(movie: movie),
          settings: RouteSettings(name: '/movie/detail')
      ));
    }
  }
}
