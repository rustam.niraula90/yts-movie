import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moviedownloader/src/model/customMeterialColor.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: CustomColor.backgroundColor,
        body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                  child: Align(
                alignment: FractionalOffset.bottomCenter,
                child: CircularProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation<Color>(CustomColor.textColor)),
              )),
              Expanded(
                  child: Align(
                alignment: FractionalOffset.bottomCenter,
                child: Padding(
                    padding: EdgeInsets.only(bottom: 60.0),
                    child: Text("YTS Mobile",
                        style: TextStyle(
                            color: CustomColor.textColor, fontSize: 50.0))),
              ))
            ]));
  }
}
