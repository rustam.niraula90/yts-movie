import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moviedownloader/src/model/customMeterialColor.dart';

class MovieImage extends StatefulWidget {
  final String imageUrl;

  MovieImage({this.imageUrl});

  @override
  State<StatefulWidget> createState() => MovieImageState(imageUrl: imageUrl);
}

class MovieImageState extends State<MovieImage> {
  final String imageUrl;

  MovieImageState({this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Image.network(
      imageUrl,
      fit: BoxFit.cover,
      height: 200,
      width: 400,
      loadingBuilder:
          (BuildContext ctx, Widget child, ImageChunkEvent loadingProgress) {
        if (loadingProgress == null) {
          return child;
        } else {
          return Center(
            child: CircularProgressIndicator(
              valueColor:
                  AlwaysStoppedAnimation<Color>(CustomColor.progressColor),
            ),
          );
        }
      },
    );
  }
}
