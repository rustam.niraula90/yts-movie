import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moviedownloader/src/model/customMeterialColor.dart';
import 'package:moviedownloader/src/model/movie.dart';
import 'package:moviedownloader/src/view/widget/movieCard.dart';
import '../service/movieService.dart';

class CustomSearchDelegate extends SearchDelegate {
  List<Movie> movieList = new List();
  String preQuery;

  @override
  ThemeData appBarTheme(BuildContext context) {
    assert(context != null);
    final ThemeData theme = Theme.of(context);
    assert(theme != null);
    return theme.copyWith(
        scaffoldBackgroundColor: CustomColor.backgroundColor,
        backgroundColor: CustomColor.backgroundColor,
        primaryColor: CustomColor.primaryBlack,
        primaryIconTheme:
            theme.primaryIconTheme.copyWith(color: CustomColor.textColor),
        textTheme: theme.textTheme.copyWith(
            title:
                theme.textTheme.title.copyWith(color: CustomColor.textColor)));
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    if (query != null && query != '') {
      if (preQuery != query) {
        preQuery = query;
        return getScaffold(FutureBuilder(
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return Center(
                  child: Text("No data found",style: TextStyle(color: CustomColor.textColor),),
                );
              }
              if (!snapshot.hasData) {
                return showLoading();
              }
              List<Movie> movieList = snapshot.data.data.movies;
              this.movieList = movieList;
              return getListView(movieList, context);
            },
            future: MovieService.getMovies(1, query, 30, "year", null)));
      } else {
        if (movieList.length > 0) {
          return Builder(builder: (context) {
            return getListView(movieList, context);
          });
        }
      }
    }
    return getScaffold(Column());
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    if (query != null && query != '') {
      return getScaffold(FutureBuilder(
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return showLoading();
            }
            if (snapshot.hasError) {
              return getScaffold(Column());
            }
            List<Movie> movieList = snapshot.data.data.movies;
            return getListView(movieList, context);
          },
          future: MovieService.getMovies(1, query, 6, "year", null)));
    } else {
      return getScaffold(Column());
    }
  }

  Widget getListView(List<Movie> movieList, context) {
    var size = MediaQuery.of(context).size;
    return GridView.count(
      crossAxisCount: 3,
      padding: const EdgeInsets.all(20.0),
      childAspectRatio: (size.width / 580),
      children: List.generate(movieList.length, (index) {
        return new MovieCard(movie: movieList[index]);
      }),
    );
  }

  Widget getScaffold(Widget child) {
    return Scaffold(
      body: child,
      backgroundColor: CustomColor.backgroundColor,
    );
  }

  Widget showLoading() {
    return Center(
      child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(CustomColor.textColor)),
    );
  }
}
