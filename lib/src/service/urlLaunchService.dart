import 'package:url_launcher/url_launcher.dart';

class UrlLaunchService {

  static void playTrailer(String ytTrailerCode) async {
    String url = 'https://www.youtube.com/watch?v=$ytTrailerCode';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  static void launchUrl(String url) async {
    print("opening url: $url");
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
