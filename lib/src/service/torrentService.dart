import 'package:moviedownloader/src/model/trackers.dart';
import 'package:moviedownloader/src/service/urlLaunchService.dart';

class TorrentService{
  static void magnetTorrentLink(
      String torrentHash,
      String movieName,
      ) async {
    movieName = Uri.encodeComponent(movieName);
    List<String> trackerList = await Trackers.getTrackerList();

    String url = 'magnet:?xt=urn:btih:$torrentHash&dn=$movieName';
    for (var i = 0; i < trackerList.length; i++) {
      url = url + "&tr=" + trackerList[i];
    }
    UrlLaunchService.launchUrl(url);
  }
}