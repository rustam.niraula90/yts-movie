import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

import '../model/movieResponse.dart';

const String BASE_URL = "https://yts.am/api/v2";

class MovieService {
  static Future<MovieResponse> getMovies(
      int page, String query, int limit, String sortBy, String genre) async {
    String url = BASE_URL + '/list_movies.json?sort_by=$sortBy&page=$page';
    if (limit != 0) url = url + '&limit=$limit';
    if (query != null) url = url + '&query_term=$query';
    if (genre != null) {
      url = url + '&genre=' + Uri.encodeComponent(genre);
    }
    print("getting movie list with url: $url");
    var response = await http.get(url);
    if (response.statusCode == 200) {
      print("Data fetched successfully with Page:" + page.toString());
      return MovieResponse.fromJson(convert.jsonDecode(response.body));
    } else {
      print("Request failed with status: ${response.statusCode}.");
      throw Exception('Failed to load movies');
    }
  }
}
