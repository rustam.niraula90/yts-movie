import 'dart:math';

import 'package:moviedownloader/src/service/movieService.dart';

import 'genre.dart';
import 'movie.dart';

class StaticMovieData {
  static Map<String, List<Movie>> movieMapByGenreForDashboard = new Map();
  static Map<String, List<Movie>> movieMapByGenreForList = new Map();
  static bool isLoaded = false;

  static Future<bool> loadData() async {
    if (!isLoaded) {
      List<String> genreList = Genre.getAllGenre();
      int count = 1;
      var futures = <Future>[];
      for (String genre in genreList) {
        futures.add(MovieService.getMovies(1, null, 48, "year", genre));
      }
      for (int i = 0; i < genreList.length; i++) {
        print("fetching data $count/" + Genre.getAllGenre().length.toString());
        List<Movie> movieList = (await futures[i]).data.movies;
        movieMapByGenreForList.putIfAbsent(genreList[i], () => (movieList));
        if (movieList.length > 10) {
          movieMapByGenreForDashboard.putIfAbsent(
              genreList[i], () => (movieList.sublist(0, 10)));
        } else {
          movieMapByGenreForDashboard.putIfAbsent(
              genreList[i], () => (movieList));
        }
        count++;
      }
      isLoaded = true;
      print("Movie Data fetched successfully");
    }
    return true;
  }

  List<Movie> loadMovieByGenreForDashboard(String genre) {
    return movieMapByGenreForDashboard[genre];
  }

  List<Movie> loadMovieByGenreForList(String genre) {
    return movieMapByGenreForList[genre];
  }

  List<Movie> getMovieSuggestion(List<String> genreList,
      int suggestionCount, String currentMovieId) {
    List<Movie> suggestionMovie = new List();
    int count = 0;
    while (count < (suggestionCount / genreList.length)) {
      for (String genre in genreList) {
        Movie suggestedMovie = getSuggestedMovie(movieMapByGenreForList[genre]);
        if (suggestedMovie.id != currentMovieId && !suggestionMovie.contains(suggestionMovie)) {
          suggestionMovie.add(suggestedMovie);
        }
      }
      count++;
    }
    return suggestionMovie;
  }

  Movie getSuggestedMovie(List<Movie> movieList) {
    return movieList[Random().nextInt(movieList.length)];
  }
}
