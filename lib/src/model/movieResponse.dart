import 'movieDate.dart';

class MovieResponse {
  final String status;
  final String status_message;
  final MovieData data;

  MovieResponse({this.status, this.status_message, this.data});

  factory MovieResponse.fromJson(Map<String, dynamic> parsedJson){
    return MovieResponse(
        status: parsedJson['status'],
        status_message: parsedJson['status_message'],
        data: MovieData.fromJson(parsedJson['data'])
    );
  }
}
