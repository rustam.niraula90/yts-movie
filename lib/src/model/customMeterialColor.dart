import 'package:flutter/material.dart';

class CustomColor {
  static MaterialColor primaryBlack = MaterialColor(
    _blackPrimaryValue,
    <int, Color>{
      50: Color(0xFF000000),
      100: Color(0xFF000000),
      200: Color(0xFF000000),
      300: Color(0xFF000000),
      400: Color(0xFF000000),
      500: Color(_blackPrimaryValue),
      600: Color(0xFF000000),
      700: Color(0xFF000000),
      800: Color(0xFF000000),
      900: Color(0xFF000000),
    },
  );
  static int _blackPrimaryValue = 0xFF000000;

  static Color backgroundColor = Colors.grey[900];
  static Color cardColor = Colors.grey[850];
  static Color textColor = Colors.white;
  static Color dividerColor = Colors.white;
  static Color progressColor = Colors.white;
}
