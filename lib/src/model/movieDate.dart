import 'movie.dart';

class MovieData {
  final String movie_count;
  final String limit;
  final String page_number;
  final List<Movie> movies;

  MovieData({
    this.movie_count, this.limit, this.page_number, this.movies
  });

  factory MovieData.fromJson(Map<String, dynamic> parsedJson) {
    return MovieData(
      movie_count: parsedJson['movie_count'].toString(),
      limit: parsedJson['limit'].toString(),
      page_number: parsedJson['page_number'].toString(),
      movies: (parsedJson['movies'] as List).map((i) => Movie.fromJson(i)).toList()
    );
  }
}