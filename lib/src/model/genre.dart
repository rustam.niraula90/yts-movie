class Genre {
  static List<String> getAllGenre() {
    List<String> genreList = new List();
    genreList.add("Action");
    genreList.add("Adventure");
    genreList.add("Animation");
    genreList.add("Biography");
    genreList.add("Comedy");
    genreList.add("Crime");
    genreList.add("Documentary");
    genreList.add("Drama");
    genreList.add("Family");
    genreList.add("Fantasy");
    genreList.add("Film-Noir");
    genreList.add("Game-Show");
    genreList.add("History");
    genreList.add("Horror");
    genreList.add("Music");
    genreList.add("Musical");
    genreList.add("Mystery");
    genreList.add("News");
    genreList.add("Reality-TV");
    genreList.add("Romance");
    genreList.add("Sci-Fi");
    genreList.add("Sport");
    genreList.add("Talk-Show");
    genreList.add("Thriller");
    genreList.add("War");
    genreList.add("Western");
    return genreList;
  }
}
