import 'package:shared_preferences/shared_preferences.dart';

class Trackers {
  static const String TRACKER_PREFERENCE_NAME = "savedTrackerList";
  static const String IS_TRACKER_INIT_PREFERENCE_NAME = "savedTracker";

  static Future<bool> initTrackerList() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getBool(IS_TRACKER_INIT_PREFERENCE_NAME) == null ||
        !prefs.getBool(IS_TRACKER_INIT_PREFERENCE_NAME)) {
      List<String> trackerList = defaultTracker();
      prefs.setStringList(TRACKER_PREFERENCE_NAME, trackerList);
      prefs.setBool(IS_TRACKER_INIT_PREFERENCE_NAME, true);
    }
    return true;
  }

  static addTracker(String tracker) async {
    final prefs = await SharedPreferences.getInstance();
    List<String> trackerList = prefs.getStringList(TRACKER_PREFERENCE_NAME);
    trackerList.add(tracker);
    prefs.setStringList(TRACKER_PREFERENCE_NAME, trackerList);
  }

  static Future<List<String>> getTrackerList() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getStringList(TRACKER_PREFERENCE_NAME);
  }

  static List<String> defaultTracker() {
    List<String> trackerList = new List(11);
    trackerList[0] = "udp://glotorrents.pw:6969/announce";
    trackerList[1] = "udp://p4p.arenabg.ch:1337";
    trackerList[2] = "udp://open.demonii.com:1337/announce";
    trackerList[3] = "udp://torrent.gresille.org:80/announce";
    trackerList[4] = "udp://p4p.arenabg.com:1337";
    trackerList[5] = "udp://tracker.openbittorrent.com:80";
    trackerList[6] = "udp://tracker.coppersurfer.tk:6969";
    trackerList[7] = "udp://tracker.opentrackr.org:1337/announce";
    trackerList[8] = "udp://tracker.leechers-paradise.org:6969";
    trackerList[9] = "udp://tracker.coppersurfer.tk:6969";
    trackerList[10] = "udp://tracker.internetwarriors.net:1337";
    return trackerList;
  }
}
