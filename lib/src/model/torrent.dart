class Torrent {
  final String url;
  final String hash;
  final String quality;
  final String type;
  final String seeds;
  final String peers;
  final String size;
  final String size_bytes;
  final String date_uploaded;
  final String date_uploaded_unix;

  Torrent(
      {this.url,
      this.hash,
      this.quality,
      this.type,
      this.seeds,
      this.peers,
      this.size,
      this.size_bytes,
      this.date_uploaded,
      this.date_uploaded_unix});

  factory Torrent.fromJson(Map<String, dynamic> parsedJson) {
    return Torrent(
      url: parsedJson['url'],
      hash: parsedJson['hash'],
      quality: parsedJson['quality'],
      type: parsedJson['type'],
      seeds: parsedJson['seeds'].toString(),
      peers: parsedJson['peers'].toString(),
      size: parsedJson['size'],
      size_bytes: parsedJson['size_bytes'].toString(),
      date_uploaded: parsedJson['date_uploaded'],
      date_uploaded_unix: parsedJson['date_uploaded_unix'].toString(),
    );
  }
}
