import 'torrent.dart';

class Movie {
  final String id;
  final String url;
  final String imdb_code;
  final String title;
  final String title_english;
  final String title_long;
  final String year;
  final String rating;
  final String runtime;
  final List<String> genres;
  final String summary;
  final String description_full;
  final String synopsis;
  final String yt_trailer_code;
  final String language;
  final String mpa_rating;
  final String background_image;
  final String background_image_original;
  final String small_cover_image;
  final String medium_cover_image;
  final String large_cover_image;
  final String state;
  final String date_uploaded;
  final String date_uploaded_unix;
  final List<Torrent> torrents;

  Movie(
      {this.id,
      this.url,
      this.imdb_code,
      this.title,
      this.title_english,
      this.title_long,
      this.year,
      this.rating,
      this.runtime,
      this.genres,
      this.summary,
      this.description_full,
      this.synopsis,
      this.yt_trailer_code,
      this.language,
      this.mpa_rating,
      this.background_image,
      this.background_image_original,
      this.small_cover_image,
      this.medium_cover_image,
      this.large_cover_image,
      this.state,
      this.date_uploaded,
      this.date_uploaded_unix,
      this.torrents});

  factory Movie.fromJson(Map<String, dynamic> parsedJson) {
    return Movie(
        id: parsedJson['id'].toString(),
        url: parsedJson['url'],
        imdb_code: parsedJson['imdb_code'],
        title: parsedJson['title'],
        title_english: parsedJson['title_english'],
        title_long: parsedJson['title_long'],
        year: parsedJson['year'].toString(),
        rating: parsedJson['rating'].toString(),
        runtime: parsedJson['runtime'].toString(),
        genres: new List<String>.from(parsedJson['genres']),
        summary: parsedJson['summary'],
        description_full: parsedJson['description_full'],
        synopsis: parsedJson['synopsis'],
        yt_trailer_code: parsedJson['yt_trailer_code'],
        language: parsedJson['language'],
        mpa_rating: parsedJson['mpa_rating'],
        background_image: parsedJson['background_image'],
        background_image_original: parsedJson['background_image_original'],
        small_cover_image: parsedJson['small_cover_image'],
        medium_cover_image: parsedJson['medium_cover_image'],
        large_cover_image: parsedJson['large_cover_image'],
        state: parsedJson['state'],
        date_uploaded: parsedJson['date_uploaded'],
        date_uploaded_unix: parsedJson['date_uploaded_unix'].toString(),
        torrents: (parsedJson['torrents'] as List).map((i) => Torrent.fromJson(i)).toList()
    );
  }
}
