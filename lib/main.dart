import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:moviedownloader/src/model/customMeterialColor.dart';
import 'package:moviedownloader/src/model/staticMovieData.dart';
import 'package:moviedownloader/src/model/trackers.dart';
import 'package:moviedownloader/src/view/recentMovie.dart';
import "package:moviedownloader/src/view/widget/splashScreen.dart";

import 'src/model/movie.dart';

List<Movie> movieList = new List();
int currentPage = 1;

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  Future<bool> initData() async {
    await StaticMovieData.loadData();
    await Trackers.initTrackerList();
    return true;
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAnalytics analytics = FirebaseAnalytics();
    return new MaterialApp(
        title: 'YTS Mobile',
        theme: new ThemeData(
          primarySwatch: CustomColor.primaryBlack,
        ),
        navigatorObservers: [
          FirebaseAnalyticsObserver(analytics: analytics),
        ],
        home: FutureBuilder(
          future: initData(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return RecentMovies();
            } else {
              return new SplashScreen();
            }
          },
        ));
  }
}
